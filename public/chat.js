

  
  function UpdateMensajes(data) { 
      var html = data.map(function(elem, index){ 
        return(`
              <div>
                  <b style="color:blue;">${elem.chat_user}</b> 
                  [<span style="color:brown;">${elem.chat_date}</span>] : 
                  <i style="color:green;">${elem.chat_text}</i>
              </div>
          `) 
      }).join(" "); 
      document.getElementById('messages').innerHTML = html; 
  }

const chat_user = document.getElementById('chat_user')
const chat_text = document.getElementById('chat_text')
const chat_send = document.getElementById('chat_send')

function addMessage(e) { 
    var mensaje = { 
        chat_user: chat_user.value, 
        chat_text: chat_text.value
    }; 
    socket.emit('new-message', mensaje); 
    chat_text.value = ''
    chat_text.focus()
    chat_send.disabled = true

    return false;
}

chat_user.addEventListener('input', () => {
    let hayEmail = chat_user.value.length
    let hayTexto = chat_text.value.length
    chat_text.disabled = !hayEmail
    chat_send.disabled = !hayEmail || !hayTexto
})

chat_text.addEventListener('input', () => {
    let hayTexto = chat_text.value.length
    chat_send.disabled = !hayTexto
})